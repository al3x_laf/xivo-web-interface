# XiVO-web-interface

This project is the core of the xivo web interface for administration.

# Development

Workflow:

1. Ensure you have all the requirements
1. Start development server
1. Start development build server

## Requirements

* A XiVO VM with latest version installed
* nodejs version 8.9.x
* npm version 5.5.x
* python
* xserver-xephyr for integration tests (`sudo apt-get install xserver-xephyr`)


## Docker

* Docker part is not updated for PHP7 as it's not currently used!

## Development server

You need a development server that will serve the php files. The main idea is to use the XiVO VM webserver that will serve the files from your git workspace.

You have several ways of serving the files, all need some kind of file sharing mechanism between your VM and your development workspace (laptop).

### Using sshfs

Here's the process one can use to develop:
1. Clone projet
1. Boot xivo VM
1. One xivo VM, 'load' the git sources :
   ``` bash
   cd /usr/share
   # save real files
   mv xivo-web-interface/ xivo-web-interface.ori
   # create dir
   mkdir xivo-web-interface
   # mount GIT
   sshfs -o allow_other <MY_USER>@<MY_LAPTOP>:/path/to/GIT/xivo.solutions/xivo-web-interface/src xivo-web-interface
   ```

Then all changes made in git are on the xivo (you need to install sshfs on your laptop and the xivo).


### Using nfs

On laptop:

1. Install nfs-kernel-server
1. Change owner of xivo-web-interface directory to nobody:nogroup
1. Add row to file /etc/exports

	/PATH_TO/xivo-web-interface/src  XIVO_IP(rw,sync,no_subtree_check)

1. sudo systemctl restart nfs-kernel-server


On XiVO:
    ``` bash
    apt-get install nfs-common
    mount -t nfs YOUR_IP:/PATH_TO/xivo-web-interface/src /usr/share/xivo-web-interface
    ```
Use IP address of the interface used for ssh connection.

Unmount by:
    ``` bash
    umount -t nfs -a
    ```
    
### Starting development build server

Part of the project is using npm/webpack thus requiring you to start a build server to package dependencies, js & css files.

``` bash
npm i 
npm run build-n-watch (run from xivo-web-interface/src )
```

If you just want to check the build is ok before pushing, you can just run: `npm run build`

## Unit testing

Tests exist in two parts:

### JS unit tests
``` bash
npm i
npm test
```

### Integration / Acceptance tests

You need Xephyr.

1. [XiVO acceptance](https://gitlab.com/xivo.solutions/xivo-acceptance)
2. 
```
cd integration_tests
pip install -r test-requirements.txt
make test-setup MANAGE_DB_DIR=/path/to/xivo-manage-db
nosetests suite
```

To change the behaviour of the integration tests, you may configure the
following environment variables:

```
DB_USER (default: asterisk)
DB_PASSWORD (default: proformatique)
DB_HOST (default: localhost)
DB_PORT (default: 15432)
DB_NAME (default: asterisk)
VIRTUAL_DISPLAY (default: 1)
WEBI_URL (default: http://localhost:8080)
CONFD_URL (default: http://localhost:19487)
DOCKER (default: 1) enables the starting/stopping of docker containers for each
    test. May be useful when developing.
```
