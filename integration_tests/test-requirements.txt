git+https://gitlab.com/xivo.solutions/xivo-provd-client.git
docker-compose==1.9.0
nose
psycopg2  # sqlalchemy engine
pyhamcrest
pyvirtualdisplay
requests<2.7  # docker-compose 1.3 refuses requests>=2.7
sqlalchemy
selenium
dataset
mock
kombu
