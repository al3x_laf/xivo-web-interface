import angular from 'angular';
import uibootstrap from 'angular-ui-bootstrap';
import angulartranslate from 'angular-translate';
import uiselect from 'ui-select';
import 'angular-ui-sortable';

import config from './xivo.config';
import run from './xivo.run';

/*Module dependencies */
import IpbxUsersController from './controllers/ipbx/users.controller';
import IpbxGroupsController from './controllers/ipbx/groups.controller';
import IpbxListRouteController from './controllers/ipbx/listroute.controller';
import IpbxAddRouteController from './controllers/ipbx/addroute.controller';
import IpbxEditRouteController from './controllers/ipbx/editroute.controller';

import IpbxMediaServerController from './controllers/ipbx/homemediaserver.controller';

import IpbxTrunkController from './controllers/ipbx/trunk.controller';
import IpbxVoicemailsController from './controllers/ipbx/voicemails.controller';
import CCAgentsController from './controllers/callcenter/agents.controller';
import CCQueuesController from './controllers/callcenter/queues.controller';
import ListMediaServerController from './controllers/configuration/management/listmediaserver.controller';
import AddMediaServerController from './controllers/configuration/management/addmediaserver.controller';
import EditMediaServerController from './controllers/configuration/management/editmediaserver.controller';
import queues from './services/queues.service';
import mediaServers from './services/mediaservers.service';
import route from './services/route.service';
import submitForm from './services/submitForm.service';
import toolbar from './services/toolbar.service';
import dynamicInput from './directives/dynamicInput.directive';
import toolbarButtons from './directives/toolbarButtons.directive';
import toolbarSearch from './directives/toolbarSearch.directive';
import onFinishRender from './directives/onFinishRender.directive';
import breadcrumb from './directives/breadcrumb.directive';
import preSubmit from './directives/preSubmit.directive';
import listTable from './directives/listTable.directive';
import ngConfirmClick from './directives/ngConfirmClick.directive';
import multipleSelect from './directives/multipleSelect.directive';
import monoSelect from './directives/monoSelect.directive';
import sortableSelect from './directives/sortableSelect.directive';
import contains from './filters/contains.filter';
import isEmpty from './filters/isEmpty.filter';
import trustHtml from './filters/trustHtml.filter';


angular.module('Xivo', [angulartranslate, uibootstrap, uiselect, 'ui.sortable'])
.config(config)
.filter('contains', contains)
.filter('isEmpty', isEmpty)
.filter('trustHtml', trustHtml)
.service('submitForm',submitForm)
.service('toolbar',toolbar)
.service('queues', queues)
.service('mediaServers', mediaServers)
.service('route', route)
.controller('IpbxUsersController', IpbxUsersController)
.controller('IpbxGroupsController', IpbxGroupsController)
.controller('IpbxListRouteController', IpbxListRouteController)
.controller('IpbxAddRouteController', IpbxAddRouteController)
.controller('IpbxEditRouteController', IpbxEditRouteController)
.controller('IpbxMediaServerController', IpbxMediaServerController)
.controller('IpbxTrunkController', IpbxTrunkController)
.controller('IpbxVoicemailsController', IpbxVoicemailsController)
.controller('CCAgentsController', CCAgentsController)
.controller('CCQueuesController', CCQueuesController)
.controller('ListMediaServerController', ListMediaServerController)
.controller('AddMediaServerController', AddMediaServerController)
.controller('EditMediaServerController', EditMediaServerController)
.directive('dynamicInput', dynamicInput)
.directive('toolbarButtons',toolbarButtons)
.directive('toolbarSearch',toolbarSearch)
.directive('onFinishRender',onFinishRender)
.directive('breadcrumb',breadcrumb)
.directive('preSubmit',preSubmit)
.directive('listTable', listTable)
.directive('ngConfirmClick', ngConfirmClick)
.directive('multipleSelect', multipleSelect)
.directive('monoSelect', monoSelect)
.directive('sortableSelect', sortableSelect)
.run(run);
