export default function monoSelect() {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/mono-select.html',
    scope: {
      label: '@',
      name: '@',
      placeholder: '@',
      list: '=',
      selected: '=',
      error: '='
    }
  };
}
