export default function multipleSelect() {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/multiple-select.html',
    scope: {
      label: '@',
      name: '@',
      placeholder: '@',
      list: '=',
      selected: '=',
      error: '='
    }
  };
}


