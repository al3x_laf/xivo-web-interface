export default function () {
  return function (array, needle) {
    return array.indexOf(needle) >= 0;
  };
}
