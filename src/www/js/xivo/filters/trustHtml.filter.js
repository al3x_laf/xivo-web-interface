export default function ($sce) {
  return function (val) {
    return $sce.trustAsHtml(val);
  };
}
