export default function isEmpty() {
  let data;
  return function (obj) {
    for (data in obj) {
      if (obj.hasOwnProperty(data)) {
        return false;
      }
    }
    return true;
  };
}
