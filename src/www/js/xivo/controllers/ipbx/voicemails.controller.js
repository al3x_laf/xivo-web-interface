export default class IpbxVoicemailsController {

  constructor($log, $scope) {
    this.$log = $log;
    this.$scope = $scope;
    this.$scope.form = { errorMessage: ""};
    this.$scope.form.errorMessage = "";
    this.$re_unescaped_semicolon = /(^|[^\\])(?=;)/;
    this.$re_single_backslash = /(^|[^\\])\\($|[^\\;nr])/;
  }

  emailBodyValid(custom_message) {
    if (angular.isDefined(this.$scope.form.emailBody)
        && ((custom_message == true && this.$scope.form.emailBody.indexOf(',') != -1)
        || this.$scope.form.emailBody.match(this.$re_unescaped_semicolon)
        || this.$scope.form.emailBody.match(this.$re_single_backslash))) {
      if (custom_message == true) {
        this.$scope.form.errorMessage = "fm_error-custom-emailbody";
      } else {
        this.$scope.form.errorMessage = "fm_error-shared-emailbody";
      }
      return false;
    }
    else {
      this.$scope.form.errorMessage = "";
      return true;
    }
  }

  sharedEmailBodyValid() {
    return this.emailBodyValid(false);
  }

  customEmailBodyValid() {
    return this.emailBodyValid(true);
  }

}
