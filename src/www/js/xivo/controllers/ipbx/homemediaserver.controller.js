export default class IpbxMediaServerController {
  constructor($scope, mediaServers, $log) {
    this.mediaServers = mediaServers;
    this.$scope = $scope;
    this.$log = $log;


    this.$scope.list = [];
    this.$scope.headers = [
      {title: 'media_servers', dataKey: 'display_name'}
      //{title: 'nb_of_users', dataKey: 'nb_of_users'}
    ];

    this.$scope.path = '/xivo/configuration/index.php/manage/mediaserver/';


    this.$scope.isLoading = true;
    mediaServers.listMediaServers()
      .then((response) => {
        this.$scope.list = response.data;
        this.$scope.isLoading = false;
        delete this.$scope.error;
      }, (error) => {
        this.$scope.error ="fm_generic_error";
        $log.error(error);
      });
  }
}
