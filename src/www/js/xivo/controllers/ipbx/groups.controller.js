export default class IpbxGroupsController {
  constructor($scope, mediaServers, $log, toolbar) {
    this.mediaServers = mediaServers;
    this.$scope = $scope;
    this.$log = $log;
    this.toolbar = toolbar;

    this.$scope.list = [];

    mediaServers.listMediaServers()
      .then((response) => {
        this.$scope.list = response.data;
      }, (error) => {
        this.toolbar.setError('fm_generic_error');
        $log.error(error);
      });
  }
}
