export default class EditMediaServerController {
  constructor(mediaServers, toolbar, $scope, $window, $log) {
    this.mediaServers = mediaServers;
    this.toolbar = toolbar;
    this.$scope = $scope;
    this.$window = $window;
    this.$log = $log;

    this.params = this.toolbar.parseParams($window.location.search);

    this.mediaServers.getMediaServer(this.params.id)
    .then((result) => {
      this.$scope.mds = result.data;
    }, (error) => {
      this.toolbar.setError('fm_generic_error');
      $log.error(error);
    });
  }

  validate() {
    this.mediaServers.updateMediaServer({
      "id": +this.params.id,
      "name": this.$scope.mds.name,
      "display_name": this.$scope.mds.display_name,
      "voip_ip": this.$scope.mds.voip_ip,
      "read_only": false
    }).then(() => {
      this.$window.location.href = '?act=list';
    }, (error) => {
      if (error.data.error === 'Duplicate') {
        this.toolbar.setError('mediaserver_duplicate_error');
      } else {
        this.toolbar.setError('fm_generic_error');
        this.$log.error(error);
      }
    });
  }
}
