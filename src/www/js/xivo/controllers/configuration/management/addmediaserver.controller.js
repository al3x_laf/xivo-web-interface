export default class AddMediaServerController {
  constructor(mediaServers, $scope, $window, $log, toolbar) {
    this.mediaServers = mediaServers;
    this.$scope = $scope;
    this.$window = $window;
    this.$log = $log;
    this.toolbar = toolbar;

    this.ip_validation = /^([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})$/;
    this.name_validation = /^[a-z0-9_\.-]+$/;
  }

  validate() {
    this.mediaServers.createMediaServer({
      "name": this.$scope.name,
      "display_name": this.$scope.display_name,
      "voip_ip": this.$scope.voip_ip,
      "read_only": false
    }).then(() => {
      this.$window.location.href = '?act=list';
    }, (error) => {
      if (error.data.error === 'Duplicate') {
        this.toolbar.setError('mediaserver_duplicate_error');
      } else {
        this.toolbar.setError('fm_generic_error');
        this.$log.error(error);
      }
    });
  }
}
