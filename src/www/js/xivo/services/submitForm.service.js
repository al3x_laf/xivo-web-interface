export default function submitForm($rootScope) {
  const submitEvt = 'submit-event';
  var form;
  var validated = false;

  const _subscribe = (scope, callback) => {
    let handler = $rootScope.$on(submitEvt, callback);
    scope.$on('$destroy', handler);
  };

  const _notify = (formElem) => {
    form = formElem;
    $rootScope.$emit(submitEvt);
  };

  const _validate = () => {
    validated = true;
  };

  const _isValidated = () => {
    return validated;
  };

  const _post = () => {
    if (_isValidated()) {
      form.submit();
    }
  };

  return {
    subscribe: _subscribe,
    notify: _notify,
    validate: _validate,
    isValidated: _isValidated,
    post: _post
  };
}
