export default function mediaServers($http) {

  const xivoApiUri = '/configmgt/api/1.0/mediaserver/';

  const _path = '/xivo/configuration/index.php/manage/mediaserver/';
  const _listMediaServers = () => {
    return $http({
      url: xivoApiUri,
      method: 'GET'
    });
  };

  const _getMediaServer = (id) => {
    return $http({
      url: xivoApiUri + id,
      method: 'GET'
    });
  };

  const _deleteMediaServer = (id) => {
    return $http({
      url: xivoApiUri + id,
      method: 'DELETE'
    });
  };

  const _createMediaServer = (mediaServer) => {
    return $http({
      url: xivoApiUri,
      method: 'POST',
      data: mediaServer
    });
  };

  const _updateMediaServer = (mediaServer) => {
    return $http({
      url: xivoApiUri,
      method: 'PUT',
      data: mediaServer
    });
  };

  return {
    listMediaServers: _listMediaServers,
    getMediaServer: _getMediaServer,
    deleteMediaServer: _deleteMediaServer,
    createMediaServer: _createMediaServer,
    updateMediaServer: _updateMediaServer,
    path: _path
  };
}
