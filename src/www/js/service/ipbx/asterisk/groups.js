/*
 * XiVO Web-Interface
 * Copyright (C) 2006-2014  Avencall
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// get available extensions
function xivo_ast_group_http_search_extension(dwsptr)
{
    context = dwho_eid('it-groupfeatures-context').value;
    new dwho.http('/service/ipbx/ui.php/pbx_settings/extension/search/?obj=group&context=' + context,
        {'callbackcomplete':    function(xhr) 
            {    dwsptr.set(xhr,dwsptr.get_search_value()); },
      'method':        'post',
      'cache':            false
        },
    {'startnum':    dwsptr.get_search_value()},
        true);
}

//get available number pool
function xivo_http_search_group_numpool()
{
    rs = jsi18n_no_number_in_context;
	context = dwho_eid('it-groupfeatures-context').value;
	helper = $('#helper-context_num_pool');
	$('#it-groupfeatures-number').autocomplete('destroy');

	$.ajax({url: '/service/ipbx/ui.php/pbx_settings/extension/search/?context='+context+'&obj=group&getnumpool=1',
		async: false,
		dataType: 'json',
		success: function(data) {
			if (data === null || (nb = data.length) === 0)
				return false;
			rs = '';
			for (var i = 0; i< nb; i++)
				rs += data[i]['numberbeg']+' - '+data[i]['numberend']+'<br>';
		}
	});
	helper.html(rs);
}

var xivo_ast_group_suggest_extension = new dwho.suggest({'requestor': xivo_ast_group_http_search_extension});

function xivo_ast_group_suggest_event_extension()
{
    xivo_ast_group_suggest_extension.set_option('result_field', 'it-groupfeatures-numberid');
    xivo_ast_group_suggest_extension.set_option('result_emptyalert', true);
    xivo_ast_group_suggest_extension.set_option('result_minlen', 0);
        
    xivo_ast_group_suggest_extension.set_field(this.id);
}

function xivo_ast_group_onload()
{
    xivo_ast_build_dialaction_array('noanswer');
    xivo_ast_build_dialaction_array('busy');
    xivo_ast_build_dialaction_array('congestion');
    xivo_ast_build_dialaction_array('chanunavail');
    xivo_ast_dialaction_onload();

    if((num = dwho_eid('it-groupfeatures-number')) !== false)
    {
        xivo_http_search_group_numpool();
        dwho.dom.add_event('focus', num, xivo_ast_group_suggest_event_extension);
        num.setAttribute('autocomplete','off');
    }

    $('#it-groupfeatures-context').change(function(){
		xivo_http_search_group_numpool();
        xivo_ast_group_suggest_event_extension();
    });
    
    $('#it-groupfeatures-number').live({
        focus: function() {
            $('#helper-context_num_pool').show();
        },
        blur: function() {
            $('#helper-context_num_pool').hide();
        }
    });
}

dwho.dom.set_onload(xivo_ast_group_onload);
