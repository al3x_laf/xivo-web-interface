<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2018  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$act		= isset($_QR['act']) === true ? $_QR['act'] : '';
$page 		= isset($_QR['page']) === true ? dwho_uint($_QR['page'],1) : 1;
$search 	= isset($_QR['search']) === true ? strval($_QR['search']) : '';

$param = array();
// default view mode == list
$param['act'] = 'list';

if($search !== '')
	$param['search'] = $search;

$contexts = false;

switch($act)
{
	case 'add':
		// add a new item (either display add form (fm_send not set)
		// 	OR save new entered item
		$appqueue = &$ipbx->get_application('queue');
		$fm_save = null;

		// we must save new item ?
		if(isset($_QR['fm_send']) 	 === true
		&& dwho_issa('qualification', $_QR) === true)
		{
			$subqualifications = array();

			// reformating subqualification values array
			// count() -1 because we skip last line (is empty & used to add new empty lines)
			$count = count($_QR['subqualification']['values']['id']) - 1;
			for($i = 0; $i < $count; $i++)
			{
				$subqualifications[] = array(
					'id'		=> $_QR['subqualification']['values']['id'][$i],
					'name'		=> $_QR['subqualification']['values']['name'][$i],
				);
			}

			// save item
			if($appqueue->qualifications_setadd($_QR['qualification']['name'], $subqualifications) === false
			|| $appqueue->qualifications_add($_QR['qualification']['name']   , $subqualifications) === false)
			{
				$error = $appqueue->get_error();

				$_TPL->set_var('info', array('qualification' => $_QR['qualification']));
				$_TPL->set_var('data', $qualifications);

				$fm_save = false;
			}
			else
				$_QRY->go($_TPL->url('callcenter/settings/qualifications'), $param);
		}

		$_TPL->set_var('fm_save', $fm_save);
		break;

	case 'edit':
		$appqueue = &$ipbx->get_application('queue');
		$fm_save  = null;

		// id not set or subqualification[id] not found => redirect to list view
		if(isset($_QR['id']) === false || ($info = $appqueue->qualifications_get($_QR['id'])) === false)
		{ $_QRY->go($_TPL->url('callcenter/settings/qualifications'), $param); }

		$act = 'edit';
		if(isset($_QR['fm_send']) 		=== true
		&& dwho_issa('qualification', $_QR) 	=== true)
		{
			$subqualifications = array();

			// count() -1 because we skip last line (is empty & used to add new empty lines)
			$count = count($_QR['subqualification']['values']['id']) - 1;
			for($i = 0; $i < $count; $i++)
			{
				$subqualifications[] = array(
					'id'		=> $_QR['subqualification']['values']['id'][$i],
					'name'		=> $_QR['subqualification']['values']['name'][$i],
				);
			}

			if($appqueue->qualifications_setedit($_QR['qualification']['name'], $subqualifications) === true
			&& $appqueue->qualifications_edit($_QR['id'], $_QR['qualification']['name'], $subqualifications) === true)
				$_QRY->go($_TPL->url('callcenter/settings/qualifications'), $param);

			$fm_save = false;

			// on update error
			$error = $appqueue->get_error();
			$info['values'] = $subqualifications;
		}

		$_TPL->set_var('data', $info['values']);

		$info = array('qualification' => array('name' => $info['name'], 'id' => $info['id']));
		$_TPL->set_var('info' , $info);
		$_TPL->set_var('fm_save' , $fm_save);
		break;

	case 'delete':
		$appqueue = &$ipbx->get_application('queue');

		if(isset($_QR['id']))
			$appqueue->qualifications_delete($_QR['id']);

		$_QRY->go($_TPL->url('callcenter/settings/qualifications'),$param);
		break;

	case 'deletes':
		$param['page'] = $page;

		if(($values = dwho_issa_val('qualifications',$_QR)) === false)
			$_QRY->go($_TPL->url('callcenter/settings/qualifications'),$param);

		$appqueue = &$ipbx->get_application('queue');
		$nb = count($values);

		for($i = 0; $i < $nb; $i++)
			$appqueue->qualifications_delete($values[$i]);

		$_QRY->go($_TPL->url('callcenter/settings/qualifications'), $param);
		break;

	case 'list':
	default:
		// list mode :: view all qualifications (modulo the filter)
		$act = 'list';
		$prevpage = $page - 1;
		$nbbypage = XIVO_SRE_IPBX_AST_NBBYPAGE;

		$appqueue = &$ipbx->get_application('queue');

		$limit = array();
		$limit[0] = $prevpage * $nbbypage;
		$limit[1] = $nbbypage;

		$list	  	= $appqueue->qualifications_getall($search, $limit);
		$total		= $appqueue->qualifications_count($search);

		if($list === false && $total > 0 && $prevpage > 0)
		{
			$param['page'] = $prevpage;
			$_QRY->go($_TPL->url('callcenter/settings/qualifications'),$param);
		}

		$_TPL->set_var('pager'	, dwho_calc_page($page, $nbbypage, $total));
		$_TPL->set_var('list'		, $list);
		$_TPL->set_var('search'	, $search);
}

$_TPL->set_var('act',$act);
//$_TPL->set_var('error',$error);
$_TPL->set_var('contexts',$contexts);

$menu = &$_TPL->get_module('menu');
$menu->set_top('top/user/'.$_USR->get_info('meta'));
$menu->set_left('left/callcenter/menu');
$menu->set_toolbar('toolbar/callcenter/settings/qualifications');

$_TPL->set_bloc('main','callcenter/settings/qualifications/'.$act);
$_TPL->set_struct('service/ipbx/'.$ipbx->get_name());
$_TPL->display('index');

?>
