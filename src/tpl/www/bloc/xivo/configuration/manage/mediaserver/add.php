<div class="b-infos b-form">
	<breadcrumb parent="{{'mediaserver' | translate}}" page="{{'add' | translate}}"></breadcrumb>
  <div ng-controller="AddMediaServerController as ctrl">
    <form action="" class="form-horizontal" accept-charser="utf-8" name="createMdsForm">
      <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : createMdsForm.name.$invalid && !createMdsForm.name.$pristine }">
        <label for="it-name" class="control-label col-sm-3" id="lb-name">{{'name' | translate}}</label>
        <div class="col-sm-3">
          <input type="text" name="name" id="it-name" class="form-control" size=30 ng-model="name" ng-pattern="ctrl.name_validation" required>
        </div>
        <div ng-show="createMdsForm.name.$invalid && !createMdsForm.name.$pristine" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
      </div>
      <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : createMdsForm.display_name.$invalid && !createMdsForm.display_name.$pristine }">
        <label for="it-display-name" class="control-label col-sm-3" id="lb-display-name">{{'display_name' | translate}}</label>
        <div class="col-sm-3">
          <input type="text" name="display_name" id="it-display-name" class="form-control" size=30 ng-model="display_name" required>
        </div>
        <div ng-show="createMdsForm.display_name.$invalid && !createMdsForm.display_name.$pristine" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
      </div>
      <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : createMdsForm.voip_ip.$invalid && !createMdsForm.voip_ip.$pristine }">
        <label for="it-voip-ip" class="control-label col-sm-3" id="lb-voip-ip">{{'voip_ip' | translate}}</label>
        <div class="col-sm-3">
          <input type="text" name="voip_ip" id="it-voip-ip" class="form-control" size=30 ng-model="voip_ip" ng-pattern="ctrl.ip_validation" required>
        </div>
        <div ng-show="createMdsForm.voip_ip.$invalid && !createMdsForm.voip_ip.$pristine" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
      </div>
      <p class="fm-paragraph-submit">
        <input ng-click="ctrl.validate()" ng-disabled="!createMdsForm.$valid" type="button" name="submit" id="it-submit" class="btn btn-primary it-submit" value="{{'fm_bt-save' | translate}}">
      </p>
    </form>
  </div>
</div>
