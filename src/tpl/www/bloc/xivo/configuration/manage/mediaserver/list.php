<div ng-controller="ListMediaServerController as ctrl">
	<list-table headers="headers" list="list" actions="actions" path="path" delete="delete(id)" loading="isLoading"
		delete-message="confirm_delete_mediaserver" form-name="fm-mediaserver-list">
	</list-table>
</div>
