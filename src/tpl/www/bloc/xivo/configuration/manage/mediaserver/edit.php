<div class="b-infos b-form">
	<breadcrumb parent="{{'mediaserver' | translate}}" page="{{'edit' | translate}}"></breadcrumb>
  <div ng-controller="EditMediaServerController as ctrl">
    <form action="" class="form-horizontal" accept-charser="utf-8" name="editMdsForm">
      <input type="hidden" name="id" id="id">
      <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : editMdsForm.name.$invalid && !editMdsForm.name.$pristine }">
        <label for="it-name" class="control-label col-sm-3" id="lb-name">{{'name' | translate}}</label>
        <div class="col-sm-3">
          <input type="text" name="name" id="it-name" class="form-control" size=30 ng-model="mds.name" ng-disabled=true>
        </div>
        <div ng-show="editMdsForm.name.$invalid && !editMdsForm.name.$pristine" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
      </div>
      <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : createMdsForm.display_name.$invalid && !createMdsForm.display_name.$pristine }">
        <label for="it-display-name" class="control-label col-sm-3" id="lb-display-name">{{'display_name' | translate}}</label>
        <div class="col-sm-3">
          <input type="text" name="display_name" id="it-display-name" class="form-control" size=30 ng-model="mds.display_name" required>
        </div>
        <div ng-show="createMdsForm.display_name.$invalid && !createMdsForm.display_name.$pristine" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
      </div>
      <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : editMdsForm.voip_ip.$invalid && !editMdsForm.voip_ip.$pristine }">
        <label for="it-voip-ip" class="control-label col-sm-3" id="lb-voip-ip">{{'voip_ip' | translate}}</label>
        <div class="col-sm-3">
          <input type="text" name="voip_ip" id="it-voip-ip" class="form-control" size=30 ng-model="mds.voip_ip" ng-pattern="ctrl.ip_validation" required>
        </div>
        <div ng-show="editMdsForm.voip_ip.$invalid && !editMdsForm.voip_ip.$pristine" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
      </div>
      <p class="fm-paragraph-submit">
        <input ng-click="ctrl.validate()" ng-disabled="!editMdsForm.$valid" type="button" name="submit" id="it-submit" class="btn btn-primary it-submit" value="{{'fm_bt-save' | translate}}">
      </p>
    </form>
  </div>
</div>
