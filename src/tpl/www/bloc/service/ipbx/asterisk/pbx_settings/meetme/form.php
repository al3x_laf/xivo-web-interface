<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2018  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$form = &$this->get_module('form');
$url = &$this->get_module('url');

$info = $this->get_var('info');
$element = $this->get_var('element');

$moh_list = $this->get_var('moh_list');
$context_list = $this->get_var('context_list');

$dhtml = &$this->get_module('dhtml');
$dhtml->write_js('var jsi18n_no_number_in_context = "'.$this->bbf('no_number_in_context').'"');

?>

<uib-tabset active="active">

  <uib-tab index="0" heading="<?=$this->bbf('smenu_general');?>">
    <?php
      echo	$form->text(array('desc'	=> $this->bbf('fm_general_name'),
              'name'	=> 'meetmefeatures[name]',
              'labelid'	=> 'meetmefeatures-name',
              'size'	=> 30,
              'default'	=> $element['meetmefeatures']['name']['default'],
              'value'	=> $info['meetmefeatures']['name'],
              'error'	=> $this->bbf_args('error',
                $this->get_var('error','meetmefeatures','name'))));
    ?>
    <?php
      echo	$form->text(array('desc'	=> $this->bbf('fm_general_number'),
              'name'	=> 'meetmefeatures[confno]',
              'labelid'	=> 'meetmefeatures-confno',
              'label'	=> 'lb-meetmefeatures-confno',
              'size'	=> 15,
              'default'	=> $element['meetmefeatures']['confno']['default'],
              'value'	=> $info['meetmefeatures']['confno'],
              'error'	=> $this->bbf_args('error',
                $this->get_var('error','meetmefeatures','confno')))),

      '<div class="dialog-helper" id="helper-context_num_pool"></div>';

      if($context_list !== false):
        echo	$form->select(array('desc'	=> $this->bbf('fm_general_context'),
                  'name'		=> 'meetmefeatures[context]',
                  'labelid'	=> 'meetmefeatures-context',
                  'key'		=> 'identity',
                  'altkey'	=> 'name',
                  'default'	=> $element['meetmefeatures']['context']['default'],
                  'selected'	=> $info['meetmefeatures']['context']),
                  $context_list);
      else:
        echo	'<div id="fd-meetmefeatures-context" class="txt-center">',
          $url->href_htmln($this->bbf('create_context'),
              'service/ipbx/system_management/context',
              'act=add'),
          '</div>';
      endif;

      if($moh_list !== false):
        echo	$form->select(array('desc'	=> $this->bbf('fm_general_musiconhold'),
                  'name'	=> 'meetmefeatures[musiconhold]',
                  'labelid'	=> 'meetmefeatures-user-musiconhold',
                  'empty'	=> true,
                  'key'	=> 'category',
                  'invalid'	=> ($this->get_var('act') === 'edit'),
                  'default'	=> ($this->get_var('act') === 'add' ?
                          $element['meetmefeatures']['musiconhold']['default'] :
                      null),
                  'selected'	=> $info['meetmefeatures']['musiconhold']),
                  $moh_list);
      endif;

            echo	$form->text(array('desc'	=> $this->bbf('fm_general_pin'),
              'name'	=> 'meetmeroom[pin]',
              'labelid'	=> 'meetmeroom-pin',
              'size'	=> 15,
              'default'	=> $element['meetmeroom']['pin']['default'],
              'value'	=> $info['meetmeroom']['pin'],
              'error'	=> $this->bbf_args('error',
                $this->get_var('error','meetmeroom','pin'))));
    ?>
    <fieldset id="fld-organizer">
    <legend><?=$this->bbf('fld_organizer');?></legend>
    <?php
      echo	$form->text(array('desc'	=> $this->bbf('fm_organizer_pinadmin'),
              'name'	=> 'meetmeroom[pinadmin]',
              'labelid'	=> 'meetmeroom-pinadmin',
              'size'	=> 15,
              'default'	=> $element['meetmeroom']['pinadmin']['default'],
              'value'	=> $info['meetmeroom']['pinadmin'],
              'error'	=> $this->bbf_args('error',
                $this->get_var('error','meetmeroom','pinadmin')))),

                $form->checkbox(array('desc'	=> $this->bbf('fm_organizer_waitingroom'),
                                      'name'	=> 'meetmefeatures[user_waitingroom]',
                                      'labelid'	=> 'meetmefeatures-user-waitingroom',
                                      'default'	=> $element['meetmefeatures']['user_waitingroom']['default'],
                                      'checked'	=> $info['meetmefeatures']['user_waitingroom'],
                                      'error'	=> $this->bbf_args('error',
                                                                   $this->get_var('error','meetmefeatures','user_waitingroom'))
                ));

    ?>
    </fieldset>
    <div class="col-sm-offset-2 fm-paragraph fm-description">
      <p>
        <label id="lb-meetmefeatures-description" for="it-meetmefeatures-description">
          <?=$this->bbf('fm_general_description');?>
        </label>
      </p>
      <?=$form->textarea(array('paragraph'	=> false,
            'label'	=> false,
            'name'		=> 'meetmefeatures[description]',
            'id'		=> 'it-meetmefeatures-description',
            'cols'		=> 60,
            'rows'		=> 5,
            'default'	=> $element['meetmefeatures']['description']['default'],
            'error'	=> $this->bbf_args('error',
              $this->get_var('error','meetmefeatures','description'))),
            $info['meetmefeatures']['description']);?>
    </div>
  </uib-tab>

  <uib-tab index="2" heading="<?=$this->bbf('smenu_advanced');?>">
<?php
    echo $form->checkbox(array('desc'    => $this->bbf('fm_advanced_noplaymsgfirstenter'),
                               'name'  => 'meetmefeatures[noplaymsgfirstenter]',
                               'labelid'       => 'meetmefeatures-noplaymsgfirstenter',
                               'default'       => $element['meetmefeatures']['noplaymsgfirstenter']['default'],
                               'checked' => $info['meetmefeatures']['noplaymsgfirstenter'])),
         $form->checkbox(array('desc'	=> $this->bbf('fm_advanced_quiet'),
                               'name'	=> 'meetmefeatures[quiet]',
                               'labelid'	=> 'meetmefeatures-quiet',
                               'default'	=> $element['meetmefeatures']['quiet']['default'],
                               'checked'	=> $info['meetmefeatures']['quiet'])),
         $form->checkbox(array('desc'	=> $this->bbf('fm_advanced_announceusercount'),
                               'name'	=> 'meetmefeatures[announceusercount]',
                               'labelid'	=> 'meetmefeatures-announceusercount',
                               'default'	=> $element['meetmefeatures']['announceusercount']['default'],
                               'checked'	=> $info['meetmefeatures']['announceusercount'])),
         $form->select(array('desc'	=> $this->bbf('fm_advanced_announcejoinleave'),
                             'name'	=> 'meetmefeatures[user_announcejoinleave]',
                             'labelid'	=> 'meetmefeatures-user-announcejoinleave',
                             'key'	=> false,
                             'bbf'	=> 'fm_advanced_announcejoinleave-opt',
                             'bbfopt'	=> array('argmode'	=> 'paramvalue'),
                             'default'	=> $element['meetmefeatures']['user_announcejoinleave']['default'],
                             'selected'	=> $info['meetmefeatures']['user_announcejoinleave']),
                       $element['meetmefeatures']['user_announcejoinleave']['value']),       
         $form->checkbox(array('desc'	=> $this->bbf('fm_advanced_record'),
                               'name'	=> 'meetmefeatures[record]',
                               'labelid'	=> 'meetmefeatures-record',
                               'default'	=> $element['meetmefeatures']['record']['default'],
                               'checked' => $info['meetmefeatures']['record'])),
         $form->text(array('desc'	=> $this->bbf('fm_advanced_maxuser'),
                           'name'	=> 'meetmefeatures[maxusers]',
                           'labelid'	=> 'meetmefeatures-maxusers',
                           'size'	=> 5,
                           'default'	=> $element['meetmefeatures']['maxusers']['default'],
                           'value'	=> $info['meetmefeatures']['maxusers'],
                           'error'	=> $this->bbf_args('error',
                                                       $this->get_var('error','meetmefeatures','maxusers')))),
         $form->text(array('desc'	=> $this->bbf('fm_advanced_preprocess_subroutine'),
                           'name'	=> 'meetmefeatures[preprocess_subroutine]',
                           'labelid'	=> 'meetmefeatures-preprocess-subroutine',
                           'size'	=> 45,
                           'default'	=> $element['meetmefeatures']['preprocess_subroutine']['default'],
                           'value'	=> $info['meetmefeatures']['preprocess_subroutine'],
                           'error'	=> $this->bbf_args('error',
                                                       $this->get_var('error','meetmefeatures','preprocess_subroutine'))));
                
    ?>
  </uib-tab>
</div>