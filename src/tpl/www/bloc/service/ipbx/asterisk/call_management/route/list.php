<div ng-controller="IpbxListRouteController as ctrl">
	<list-table headers="headers" list="list" actions="actions" path="path" delete="delete(id)" loading="isLoading"
		delete-message="confirm_delete_route" no-content-message="no_route" form-name="fm-route-list" with-id="true">
	</list-table>
</div>
