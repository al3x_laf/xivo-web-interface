<div class="b-infos b-form">
	<breadcrumb parent="{{'route' | translate}}" page="{{'edit' | translate}}"></breadcrumb>
  <div ng-controller="IpbxEditRouteController as ctrl">
		<uib-tabset active="active">
			<uib-tab index="0" heading="{{'general' | translate}}">
				<form action="" class="form-horizontal" accept-charser="utf-8" name="editRouteForm">

					<fieldset id="fld-route-definition">
						<legend>{{'definition' | translate}}</legend>

						<div class="form-group form-group-sm required form-inline spacer" ng-class="{ 'has-error' : (errors | contains: 'priority') }">
							<label for="it-priority" class="control-label col-sm-3">{{'priority' | translate}}</label>
							<div class="col-sm-7">
								<input type="number" name="priority" id="it-priority" class="form-control thin-input" size=4 ng-model="routeForm.priority" min="1" max="99999">
							</div>
							<div class="col-sm-4 col-sm-offset-3">
								<div ng-show="(errors | contains: 'priority')" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
							</div>
						</div>

						<dynamic-input class="form-group required" name="call_pattern" columns="columns"
							inputs="routeForm.dialpattern" error="(errors | contains: 'dialpattern')">
						</dynamic-input>

						<div class="form-group form-group-sm">
							<label for="it-internal" class="control-label col-sm-3">{{'internal' | translate}}</label>
							<div class="col-sm-1">
								<input type="checkbox" name="internal" id="it-internal" ng-model="routeForm.internal">
							</div>
						</div>

						<hr />

						<multiple-select class="form-group" label="mediaserver" name="display_name" placeholder="empty_all" list="prevForm.mediaserver"
							selected="routeForm">
						</multiple-select>

						<multiple-select class="form-group" label="context" name="name" placeholder="empty_all" list="prevForm.context"
							selected="routeForm">
						</multiple-select>

					</fieldset>

					<fieldset id="fld-route-destination">
						<legend>{{'destination' | translate}}</legend>

						<sortable-select ng-if="ctrl.hasTrunk()" class="form-group required spacer" label="trunk" list="prevForm.trunk"
							selected="routeForm.trunk" error="(errors | contains: 'trunk')">
						</sortable-select>

						<div class="form-group form-group-sm required spacer" ng-if="!ctrl.hasTrunk()">
							<div class="col-sm-4 col-sm-offset-3">
								<a href="/service/ipbx/index.php/trunk_management/sip/?act=add" title="{{'create_trunk' | translate}}" target="_blank">{{'create_trunk' | translate}}</a>
							</div>
							<div class="col-sm-4 col-sm-offset-3" ng-show="(errors | contains: 'trunk')">
								<div class="fm-error-icon">{{'fm_field_required' | translate}}</div>
							</div>
						</div>

	          <div class="form-group form-group-sm">
	            <label for="it-subroutine" class="control-label col-sm-3">{{'subroutine' | translate}}</label>
	            <div class="col-sm-9">
	              <input type="text" ng-model="routeForm.subroutine"  class="form-control large-input" id="it-subroutine"/>
	            </div>
	          </div>

						<hr />

						<div class="form-group form-group-sm form-inline">
							<label for="it-description" class="control-label col-sm-3">{{'description' | translate}}</label>
							<div class="col-sm-7">
								<textarea rows="4" ng-model="routeForm.description" placeholder="{{'description_route' | translate}}" id="it-description" class="form-control large-input"></textarea>
							</div>
						</div>
					</fieldset>

					<p class="fm-paragraph-submit">
						<input ng-click="ctrl.validate()" ng-disabled="!editRouteForm.$valid" type="button" name="submit" id="it-submit" class="btn btn-primary it-submit" value="{{'fm_bt-save' | translate}}">
					</p>
				</form>
			</uib-tab>
			<uib-tab index="1" heading="{{'right_and_schedule' | translate}}">
        <form action="" class="form-horizontal" accept-charser="utf-8" name="createRouteForm">

					<multiple-select label="right" name="name" placeholder="select" list="prevForm.right"
            selected="routeForm">
          </multiple-select>

          <mono-select label="schedule" name="name" placeholder="select" list="prevForm.schedule"
            selected="routeForm">
          </mono-select>

          <p class="fm-paragraph-submit">
            <input ng-click="ctrl.validate()" type="button" name="submit" id="it-submit" class="btn btn-primary it-submit" value="{{'fm_bt-save' | translate}}">
          </p>

        </form>
      </uib-tab>

		</uib-tabset>
  </div>
</div>
