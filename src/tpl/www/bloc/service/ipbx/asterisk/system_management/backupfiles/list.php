<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2014  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$url = &$this->get_module('url');
$form = &$this->get_module('form');

$pager = $this->get_var('pager');
$act = $this->get_var('act');

$page = $url->pager($pager['pages'],
		    $pager['page'],
		    $pager['prev'],
		    $pager['next'],
		    'service/ipbx/system_management/backupfiles',
		    array('act' => $act));
?>
<div class="b-list">
<?php
	if($page !== ''):
		echo '<div class="b-page">',$page,'</div>';
	endif;
?>
<table class="table table-condensed table-striped table-hover table-bordered" id="table-main-listing">
	<tr class="sb-top">
		<th class="th-center"><?=$this->bbf('col_file');?></th>
		<th class="th-center"><?=$this->bbf('col_size');?></th>
		<th class="th-center"><?=$this->bbf('col_date');?></th>
	</tr>
<?php
	if(($list = $this->get_var('list')) === false || ($nb = count($list)) === 0):
?>
	<tr class="sb-content">
		<td colspan="5" class="td-single"><?=$this->bbf('no_file');?></td>
	</tr>
<?php
	else:
		for($i = $pager['beg'],$j = 0;$i < $pager['end'] && $i < $pager['total'];$i++,$j++):

			$ref = &$list[$i];
			$size = dwho_size_iec($ref['stat']['size']);
?>
	<tr onmouseover="this.tmp = this.className; this.className = 'sb-content l-infos-over';"
	    onmouseout="this.className = this.tmp;"
	    class="sb-content l-infos-<?=(($j % 2) + 1)?>on2">
		<td class="td-left txt-left">
		<?=$url->href_html($ref['name'], '/backup/'.$ref['name']);?>
		</td>
		<td class="td-center"><?=$this->bbf('size_iec_'.$size[1],$size[0]);?></td>
		<td class="td-right"><?=dwho_i18n::strftime_l($this->bbf('date_format_yymmddhhii'),
									  null,
									  $ref['stat']['mtime']);?></td>
	</tr>
<?php
		endfor;
	endif;
?>
</table>
<?php
	if($page !== ''):
		echo '<div class="b-page">',$page,'</div>';
	endif;
?>
</div>
