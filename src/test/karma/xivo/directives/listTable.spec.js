describe('list-table directive', () => {
  var $compile;
  var $rootScope;

  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it('Generate html for defined actions', () => {

    $rootScope.headers = ['id', 'name', 'voip_ip'];
    $rootScope.actions = ['edit', 'delete'];
    $rootScope.path = '/test/path/';
    $rootScope.list = [
      {
        "id": 1,
        "name": "mds1",
        "voip_ip": "10.52.0.10",
        "read_only": false
      },
      {
        "id": 2,
        "name": "mds2",
        "voip_ip": "10.52.0.20",
        "read_only": false
      }
    ];

    var element = $compile('<list-table headers="headers" list="list" actions="actions" path="path"></list-table>')($rootScope);
    $rootScope.$digest();
    expect(element.html()).toContain('ng-href="/test/path/?act=edit&amp;id=2"');
    expect(element.html()).toContain('ng-click="delete({id: elem.id})"');
  });

  it('Generates values in lists in the html', () => {
    
    $rootScope.headers = ['priority', 'name', 'pattern', 'context', 'site', 'trunk'];
    $rootScope.actions = ['edit', 'delete'];
    $rootScope.path = '/test/path/';
    $rootScope.list = [
      {
        "priority": 1,
        "name": "Default",
        "pattern": "+33X",
        "context": "+",
        "site": "MDS 0",
        "trunk": "My Trunk"
      },
    ];

    var element = $compile('<list-table headers="headers" list="list" actions="actions" path="path"></list-table>')($rootScope);
    $rootScope.$digest();
        
    expect(element.find('tr').eq(1).find('td').eq(2).find('span').html().replace(/\s/g,'')).toEqual('+33X');  
  });

  it('check when list item is an Array', () => {
    var tableValue = ["+33X", "XXXX"];
    var singleValue = "+33X";

    let elem = angular.element('<list-table headers="headers" list="list"></list-table>');
    let scope = $rootScope.$new();

    let elemCompiled = $compile(elem)(scope);
    $rootScope.$digest();

    let isolatedScope =  elemCompiled.isolateScope();

    expect(isolatedScope.isArray(singleValue)).toEqual(false);
    expect(isolatedScope.isArray(tableValue)).toEqual(true);
  });

  it('Generates different values under Pattern and Context for one Priority', () => {
    
    $rootScope.headers = ['priority', 'name', 'pattern', 'context', 'site', 'trunk'];
    $rootScope.actions = ['edit', 'delete'];
    $rootScope.path = '/test/path/';
    $rootScope.list = [
      {
        "priority": 1,
        "name": "Default",
        "pattern": ["+33X", "XXXX"],
        "context": "Default",
        "site": "MDS 0",
        "Trunk": "My Trunk"
      },
    ];

    var element = $compile('<list-table headers="headers" list="list" actions="actions" path="path"></list-table>')($rootScope);
    $rootScope.$digest();
    let tableElem = (element.find('tr').eq(1).find('td').eq(2).find('span').html().replace(/\s/g,''));
    expect(tableElem).toContain('<table');  
    expect(tableElem).toContain('+33X');  
    expect(tableElem).toContain('XXXX');
    expect(tableElem).toContain('</table>');    
  });

  it('Generates header and data index from header attribute with string array', function() {
    $rootScope.headers = ['testHeader1', 'name'];
    $rootScope.list = [
      {
        'testHeader1': 'testValue'
      }
    ];

    var element = $compile('<list-table headers="headers" list="list" actions="actions"></list-table>')($rootScope);
    $rootScope.$digest();
    let tableElem = (element.find('tr').eq(0).find('th').eq(0).html().replace(/\s/g,''));
    expect(tableElem).toContain('testHeader1');    
    let columnValue = (element.find('tr').eq(1).find('td').eq(0).html().replace(/\s/g,''));
    expect(columnValue).toContain('testValue');    

  });

  it('Generates header and data index from header attribute with object array', function() {
    $rootScope.headers = [{title: 'testHeader1title', dataKey: 'testHeaderDataKey'}];
    $rootScope.list = [
      {
        "testHeaderDataKey": "Default"
      },      
    ];

    var element = $compile('<list-table headers="headers" list="list" actions="actions"></list-table>')($rootScope);
    $rootScope.$digest();
    let tableElem = (element.find('tr').eq(1).find('td').eq(0).html().replace(/\s/g,''));
    expect(tableElem).toContain('Default');    
  });


});
