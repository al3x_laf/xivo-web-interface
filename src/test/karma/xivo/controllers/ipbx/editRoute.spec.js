describe('edit-route controller', () => {
  var $controller;
  var $rootScope;
  var $q;

  var scope;
  var ctrl;
  var route;
  var toolbar;

  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function(_$q_,_$controller_,_$rootScope_, _route_, _toolbar_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $q = _$q_;

    scope = $rootScope.$new();
    route = _route_;
    toolbar = _toolbar_;
  }));

  it('Checks that the controller exists', () => {
    ctrl = $controller('IpbxEditRouteController', {'$scope': scope});
    expect(ctrl).toBeDefined();
  });

  it('Gets the previous route at instanciation', () => {
    spyOn(toolbar, 'parseParams').and.returnValue({id: 1});

    spyOn(route, 'getRoute').and.callFake(() => {
      return $q.defer().promise;
    });
    $rootScope.$digest();
    ctrl = $controller('IpbxEditRouteController', {'$scope': scope});

    expect(route.getRoute).toHaveBeenCalledWith(1);
  });

});
