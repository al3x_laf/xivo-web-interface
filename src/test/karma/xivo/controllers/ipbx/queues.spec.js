describe("queues controller", function() {

  var $controller;
  var $rootScope;
  var $scope;
  var $window;
  var $q;

  var ctrl;
  var queues;
  var toolbar;
  var submitForm;

  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject(function(_$controller_, _$rootScope_, _$window_, _$q_, _queues_, _toolbar_, _submitForm_){
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $window = _$window_;
    $q = _$q_;

    queues = _queues_;
    toolbar = _toolbar_;
    submitForm = _submitForm_;
  }));

  const instantiate = () => {
    spyOn(toolbar, 'parseParams').and.returnValue({act:'edit', id:'1'});
    spyOn(queues, 'getRecConfig').and.callFake(() => {
      let defer = $q.defer();
      defer.resolve(true);
      return defer.promise;
    });

    ctrl = $controller('CCQueuesController', {'$scope': $scope, '$window': $window, 'queues' : queues, 'toolbar': toolbar, 'submitForm': submitForm});
  };

  it("can be instantiated", () => {
    instantiate();
    expect(ctrl).toBeDefined();
  });

  it("call queue service at laoding to populate recording info", () => {
    spyOn(submitForm, 'subscribe');
    instantiate();

    expect(submitForm.subscribe).toHaveBeenCalled();
    expect(queues.getRecConfig).toHaveBeenCalledWith('1');
  });

  it("call queue service to set recording info at submit", () => {
    spyOn(submitForm, 'validate');
    spyOn(submitForm, 'post');
    spyOn(queues, 'setRecConfig').and.callFake(() => {
      let defer = $q.defer();
      defer.resolve(true);
      return defer.promise;
    });
    instantiate();
    ctrl.onSubmit();
    $rootScope.$digest();

    expect(queues.setRecConfig).toHaveBeenCalledWith('1', {});
    expect(submitForm.validate).toHaveBeenCalled();
    expect(submitForm.post).toHaveBeenCalled();
  });
});
