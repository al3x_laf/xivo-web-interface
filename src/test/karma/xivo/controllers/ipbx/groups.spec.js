describe('groups controller', () => {
  var $controller;
  var $rootScope;
  var $q;
  var mediaServers;

  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.inject((_$controller_, _$rootScope_, _mediaServers_,_$q_) => {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $q = _$q_;
    mediaServers = _mediaServers_;
  }));

  it('can be instantiated', () => {
    var $scope = $rootScope.$new();
    let ctrl = $controller('IpbxGroupsController', { $scope: $scope });
    expect(ctrl).toBeDefined();
    expect($scope.list).toEqual([]);
  });

  it('get mediaservers on load', () => {
    var $scope = $rootScope.$new();
    let list = [{'name': 'mds0', 'display_name': 'MDS Main'}, {'name': 'mds1', 'display_name': 'MDS 1'}];
    spyOn(mediaServers, 'listMediaServers').and.callFake(() => {
      let defer = $q.defer();
      defer.resolve({'data': list});
      return defer.promise;
    });

    $controller('IpbxGroupsController', { $scope: $scope, mediaServers: mediaServers });
    $rootScope.$digest();
    expect($scope.list).toEqual(list);
    expect(mediaServers.listMediaServers).toHaveBeenCalledWith();
  });
});
