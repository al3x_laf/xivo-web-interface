describe('IpbxVoicemails controller', () => {
  var $rootScope;
  var scope;
  var ctrl;

  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject((_$rootScope_, _$controller_) =>{
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
    ctrl = _$controller_('IpbxVoicemailsController', {
      '$scope': scope });
  }));

  it('it can be instantiated', () => {
    expect(ctrl).toBeDefined();
  });

  it('validates custom email body without comma', () => {
    scope.form = {emailBody: "body\n without comma"};
    expect(ctrl.customEmailBodyValid()).toEqual(true);
  });

  it('validates custom email body with comma', () => {
    scope.form = {emailBody: "body,\n with comma"};
    expect(ctrl.customEmailBodyValid()).toEqual(false);
  });

  it('validates custom email body with escaped semicolon', () => {
    scope.form = {emailBody: "body\\;\n with semicolon"};
    expect(ctrl.customEmailBodyValid()).toEqual(true);
  });

  it('validates custom email body with unescaped semicolon', () => {
    scope.form = {emailBody: "body;\n with semicolon"};
    expect(ctrl.customEmailBodyValid()).toEqual(false);
  });

  it('validates custom email body with semicolon as first letter', () => {
    scope.form = {emailBody: ";body\n with semicolon"};
    expect(ctrl.customEmailBodyValid()).toEqual(false);
  });

  it('validates shared email body without comma', () => {
    scope.form = {emailBody: "body\n without comma"};
    expect(ctrl.sharedEmailBodyValid()).toEqual(true);
  });

  it('validates shared email body with comma', () => {
    scope.form = {emailBody: "body,\n with comma"};
    expect(ctrl.sharedEmailBodyValid()).toEqual(true);
  });

  it('validates shared email body with single backspace', () => {
    scope.form = {emailBody: "body with \\ single backspace"};
    expect(ctrl.sharedEmailBodyValid()).toEqual(false);
  });

  it('validates shared email body with single backspace as first letter', () => {
    scope.form = {emailBody: "\\body with single backspace"};
    expect(ctrl.sharedEmailBodyValid()).toEqual(false);
  });

  it('validates shared email body with single backspace as last letter', () => {
    scope.form = {emailBody: "body with single backspace\\"};
    expect(ctrl.sharedEmailBodyValid()).toEqual(false);
  });

  it('validates shared email body with escaped backspace', () => {
    scope.form = {emailBody: "\\\\body with \\\\ escaped backspace\\\\"};
    expect(ctrl.sharedEmailBodyValid()).toEqual(true);
  });

});
