describe('mediaServers service', () => {
  var $httpBackend;
  var mediaServers;

  var url = '/configmgt/api/1.0/mediaserver/';

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject((_mediaServers_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    mediaServers = _mediaServers_;
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('gets list of mediaServers', () => {
    let response = [
      {'mds1': 'mds1'},
      {'mds2': 'mds2'}
    ];

    $httpBackend.whenGET(url).respond(response);
    mediaServers.listMediaServers();
    $httpBackend.expect('GET', url);
  });

  it('gets a mediaServer', () => {
    let response = {
      'name': 'mds1'
    };

    $httpBackend.whenGET(url + '15').respond(response);
    mediaServers.getMediaServer(15);
    $httpBackend.expect('GET', url + '15');
  });

  it('delete a mediaServer', () => {
    let response = {
      'name': 'mds1'
    };

    $httpBackend.whenDELETE(url + '15').respond(response);
    mediaServers.deleteMediaServer(15);
    $httpBackend.expect('DELETE', url + '15');
  });

  it('create a mediaServer', () => {
    let mediaServer = {
      'name' : 'mds1'
    };

    let response = {
      'id': '42',
      'name' : 'mds1'
    };

    $httpBackend.whenPOST(url).respond(response);
    mediaServers.createMediaServer(mediaServer);
    $httpBackend.expect('POST', url);
  });

  it('update a mediaServer', () => {
    let mediaServer = {
      'name' : 'mds1'
    };

    let response = {
      'id': '42',
      'name' : 'mds1'
    };

    $httpBackend.whenPUT(url).respond(response);
    mediaServers.updateMediaServer(42, mediaServer);
    $httpBackend.expect('PUT', url);
  });
});