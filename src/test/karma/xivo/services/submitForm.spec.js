describe('submitForm service', () => {
  var $rootScope;
  var submitForm;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject((_submitForm_, _$rootScope_) => {
    $rootScope = _$rootScope_;
    submitForm = _submitForm_;
  }));

  it('subscribes to submit event', () => {
    let callback = () => {};
    let scope = $rootScope.$new();
    spyOn($rootScope, '$on');
    submitForm.subscribe(scope, callback);

    expect($rootScope.$on).toHaveBeenCalledWith('submit-event', callback);
  });

  it('notifies when form submit', () => {
    spyOn($rootScope, '$emit');
    submitForm.notify();

    expect($rootScope.$emit).toHaveBeenCalledWith('submit-event');
  });

  it('validates a form', () => {
    expect(submitForm.isValidated()).toBe(false);
    submitForm.validate();
    expect(submitForm.isValidated()).toBe(true);
  });

  it('posts the form if validated', () => {
    let formMock = {
      submit: () => {}
    };
    spyOn(formMock, 'submit');
    submitForm.notify(formMock);
    submitForm.validate();
    submitForm.post();

    expect(formMock.submit).toHaveBeenCalled();
  });

});
