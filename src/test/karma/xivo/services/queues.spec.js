describe('queues service', () => {
  var $httpBackend;
  var queues;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject((_queues_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    queues = _queues_;

    $httpBackend.whenGET('/configmgt/api/1.0/recording/config/1').respond('');
    $httpBackend.whenPOST('/configmgt/api/1.0/recording/config/1').respond('');
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('gets recording config', () => {
    queues.getRecConfig('1');
    $httpBackend.expect('GET', '/configmgt/api/1.0/recording/config/1');
  });

  it('sets recording config', () => {
    queues.setRecConfig('1');
    $httpBackend.expect('POST', '/configmgt/api/1.0/recording/config/1');
  });

});
