<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2014  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

xivo_object::required(array('abstract','abstract.inc'),true);
xivo_object::required(array('bus','publisher.inc'),true);

class xivo_object_request_handlers extends xivo_object_abstract
{
	protected $_name      = 'request_handlers';
	protected $_sysconfd  = false;
	protected $_bus_msg_factory = null;

	public function __construct(&$xobj,$param=array())
	{

		$this->_publisher = Publisher::from_config();
		$this->_bus_msg_queue = new MessageQueue();
		$this->_bus_msg_factory = new MessageFactory();
		if(($this->_sysconfd = &$xobj->get_module('sysconfd')) === false)
			return(false);

	}

	public function exec_cmd($cmds)
	{
		if(dwho_constant('XIVO_WEBI_CONFIGURED',true) === false)
			return(null);

		if (isset($cmds['ipbx'])) {
			$bus_msg = $this->_bus_msg_factory->new_ipbx_reload_msg($cmds['ipbx']);
			$this->_bus_msg_queue->add_msg($bus_msg);
			$this->_publisher->flush();
			unset($cmds['ipbx']);
		}

		if ($cmds) {
			if ($this->_sysconfd->request_post('/exec_request_handlers', $cmds) === false
			|| $this->_sysconfd->last_status_code() !== 200) {
				return(false);
			}
		}
		return(true);
	}
}

?>
