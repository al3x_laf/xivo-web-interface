<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

class xivo_service_asterisk_userfeatures_identity
{
	function _prepare_identity(&$data,$full=true,$summary=null)
	{
		if(dwho_has_len($summary['context']) === false)
			$summary['context'] = 'default';

		$data['fullname'] = trim(trim($data['firstname']).' '.trim($data['lastname']));

		if($data['fullname'] === '')
			$data['fullname'] = '-';

		$data['identity'] = $data['fullname'];

		if((bool) $full === false)
			return($data);
		else if(dwho_has_len($summary['extension']) === true && dwho_has_len($summary['configregistrar']) === true)
			$data['identity'] .= ' '.$summary['extension'].'@'.$summary['configregistrar'] . ' ['.$summary['context'].']';
		else
			$data['identity'] .= ' ['.$summary['context'].']';

		return($data);
	}

	function mk_identity($arr,$full=true)
	{
		if(is_array($arr) === false
		|| isset($arr['firstname'],$arr['lastname']) === false)
			return(false);

		if(isset($arr['number']) === false)
			$arr['number'] = '';

		if(isset($arr['context']) === false)
			$arr['context'] = '';

		$this->_prepare_identity($arr,$full);

		return($arr['identity']);
	}
}

?>
